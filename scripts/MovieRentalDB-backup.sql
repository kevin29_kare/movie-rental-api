--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6 (Ubuntu 11.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.6 (Ubuntu 11.6-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: movie; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.movie (
    id integer NOT NULL,
    title character varying NOT NULL,
    description character varying NOT NULL,
    poster character varying,
    stock integer NOT NULL,
    trailer character varying,
    "salePrice" integer NOT NULL,
    "rentPrice" integer NOT NULL,
    likes integer DEFAULT 0 NOT NULL,
    "isActive" boolean DEFAULT true NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT "CHK_210bc71aafe5c5dd956039e46c" CHECK (("rentPrice" >= 0)),
    CONSTRAINT "CHK_3c9065fb448de8b8e1ba994bec" CHECK ((likes >= 0)),
    CONSTRAINT "CHK_43b7551a73b6d2930cac8067f6" CHECK (("salePrice" >= 0)),
    CONSTRAINT "CHK_5abd9c6fb8fc47175544e5d5e1" CHECK ((stock >= 0))
);


ALTER TABLE public.movie OWNER TO kevin;

--
-- Name: movie_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.movie_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_id_seq OWNER TO kevin;

--
-- Name: movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.movie_id_seq OWNED BY public.movie.id;


--
-- Name: movie_tags_tag; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.movie_tags_tag (
    "movieId" integer NOT NULL,
    "tagId" integer NOT NULL
);


ALTER TABLE public.movie_tags_tag OWNER TO kevin;

--
-- Name: order; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public."order" (
    id integer NOT NULL,
    total integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "userId" integer NOT NULL,
    CONSTRAINT "CHK_1297c4dcd6a7077c27a83bb36c" CHECK ((total >= 0))
);


ALTER TABLE public."order" OWNER TO kevin;

--
-- Name: order_detail; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.order_detail (
    id integer NOT NULL,
    quantity integer NOT NULL,
    "unitPrice" integer NOT NULL,
    "subTotal" integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "orderId" integer NOT NULL,
    "movieId" integer NOT NULL,
    CONSTRAINT "CHK_57585e3fab24f132acf9b68d42" CHECK (("unitPrice" >= 0)),
    CONSTRAINT "CHK_ef96bce219ed634abe33758341" CHECK (("subTotal" >= 0))
);


ALTER TABLE public.order_detail OWNER TO kevin;

--
-- Name: order_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.order_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_detail_id_seq OWNER TO kevin;

--
-- Name: order_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.order_detail_id_seq OWNED BY public.order_detail.id;


--
-- Name: order_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_id_seq OWNER TO kevin;

--
-- Name: order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.order_id_seq OWNED BY public."order".id;


--
-- Name: rent; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.rent (
    id integer NOT NULL,
    total integer NOT NULL,
    "returnDate" timestamp without time zone NOT NULL,
    returned boolean DEFAULT false NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "userId" integer NOT NULL,
    CONSTRAINT "CHK_3ec91b293c0c2be6d03d1c9c3f" CHECK ((total >= 0))
);


ALTER TABLE public.rent OWNER TO kevin;

--
-- Name: rent_detail; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.rent_detail (
    id integer NOT NULL,
    quantity integer NOT NULL,
    "unitPrice" integer NOT NULL,
    "subTotal" integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "rentId" integer NOT NULL,
    "movieId" integer NOT NULL,
    CONSTRAINT "CHK_0bce42f4d45906111a922d4616" CHECK (("unitPrice" >= 0)),
    CONSTRAINT "CHK_206e9ca276f683a9453228fc91" CHECK (("subTotal" >= 0))
);


ALTER TABLE public.rent_detail OWNER TO kevin;

--
-- Name: rent_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.rent_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_detail_id_seq OWNER TO kevin;

--
-- Name: rent_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.rent_detail_id_seq OWNED BY public.rent_detail.id;


--
-- Name: rent_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.rent_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rent_id_seq OWNER TO kevin;

--
-- Name: rent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.rent_id_seq OWNED BY public.rent.id;


--
-- Name: rol; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.rol (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.rol OWNER TO kevin;

--
-- Name: rol_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.rol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rol_id_seq OWNER TO kevin;

--
-- Name: rol_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.rol_id_seq OWNED BY public.rol.id;


--
-- Name: tag; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.tag (
    id integer NOT NULL,
    name character varying NOT NULL,
    "isActive" boolean DEFAULT true NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.tag OWNER TO kevin;

--
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_id_seq OWNER TO kevin;

--
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.tag_id_seq OWNED BY public.tag.id;


--
-- Name: token; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public.token (
    id integer NOT NULL,
    token character varying NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "userId" integer NOT NULL
);


ALTER TABLE public.token OWNER TO kevin;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.token_id_seq OWNER TO kevin;

--
-- Name: token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.token_id_seq OWNED BY public.token.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: kevin
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying(128) NOT NULL,
    password character varying(60) NOT NULL,
    "firstName" character varying NOT NULL,
    "lastName" character varying NOT NULL,
    email character varying(128) NOT NULL,
    "isActive" boolean DEFAULT true NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "rolId" integer NOT NULL
);


ALTER TABLE public."user" OWNER TO kevin;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO kevin;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: movie id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.movie ALTER COLUMN id SET DEFAULT nextval('public.movie_id_seq'::regclass);


--
-- Name: order id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."order" ALTER COLUMN id SET DEFAULT nextval('public.order_id_seq'::regclass);


--
-- Name: order_detail id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.order_detail ALTER COLUMN id SET DEFAULT nextval('public.order_detail_id_seq'::regclass);


--
-- Name: rent id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent ALTER COLUMN id SET DEFAULT nextval('public.rent_id_seq'::regclass);


--
-- Name: rent_detail id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent_detail ALTER COLUMN id SET DEFAULT nextval('public.rent_detail_id_seq'::regclass);


--
-- Name: rol id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rol ALTER COLUMN id SET DEFAULT nextval('public.rol_id_seq'::regclass);


--
-- Name: tag id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.tag ALTER COLUMN id SET DEFAULT nextval('public.tag_id_seq'::regclass);


--
-- Name: token id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.token ALTER COLUMN id SET DEFAULT nextval('public.token_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: movie; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.movie (id, title, description, poster, stock, trailer, "salePrice", "rentPrice", likes, "isActive", "createdAt", "updatedAt") FROM stdin;
2	Endgame	The final battle begins	\N	20	https://www.youtube.com/watch?v=TcMBFSGVi1c	10	8	0	t	2020-01-09 11:17:16.998346	2020-01-09 18:11:13.597893
4	Civil War	Lets meet with winter soldier	\N	30	https://www.youtube.com/watch?v=dKrVegVI0Us	50	5	0	t	2020-01-09 12:11:33.837876	2020-01-10 12:24:12.65685
1	Avatar	Adventure in Pandora	\N	40	\N	30	12	0	t	2020-01-09 11:16:36.790483	2020-01-09 18:40:12.50128
3	Adventure in Hogwarts	Lets meet with fantastic beasts	\N	25	\N	50	5	0	t	2020-01-09 11:18:04.963231	2020-01-10 12:24:12.676435
\.


--
-- Data for Name: movie_tags_tag; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.movie_tags_tag ("movieId", "tagId") FROM stdin;
1	1
1	2
2	1
2	3
2	4
4	2
4	4
4	5
3	2
3	4
3	5
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public."order" (id, total, "createdAt", "updatedAt", "userId") FROM stdin;
1	100	2020-01-09 11:18:26.109983	2020-01-09 11:18:26.109983	1
2	35	2020-01-09 18:40:12.463208	2020-01-09 18:40:12.463208	2
3	60	2020-01-09 18:43:32.883528	2020-01-09 18:43:32.883528	2
\.


--
-- Data for Name: order_detail; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.order_detail (id, quantity, "unitPrice", "subTotal", "createdAt", "updatedAt", "orderId", "movieId") FROM stdin;
1	4	10	40	2020-01-09 11:18:26.124702	2020-01-09 11:18:26.124702	1	1
2	4	15	60	2020-01-09 11:18:26.124702	2020-01-09 11:18:26.124702	1	2
3	2	10	20	2020-01-09 18:40:12.473466	2020-01-09 18:40:12.473466	2	1
4	1	15	15	2020-01-09 18:40:12.473466	2020-01-09 18:40:12.473466	2	3
5	3	10	30	2020-01-09 18:43:32.897256	2020-01-09 18:43:32.897256	3	4
6	2	15	30	2020-01-09 18:43:32.897256	2020-01-09 18:43:32.897256	3	3
\.


--
-- Data for Name: rent; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.rent (id, total, "returnDate", returned, "createdAt", "updatedAt", "userId") FROM stdin;
2	65	2020-01-08 18:17:57.496	t	2020-01-09 16:54:48.538392	2020-01-09 18:10:01.281041	1
7	125	2020-02-09 18:17:57.496	t	2020-01-09 18:48:44.604042	2020-01-09 18:49:42.783809	2
\.


--
-- Data for Name: rent_detail; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.rent_detail (id, quantity, "unitPrice", "subTotal", "createdAt", "updatedAt", "rentId", "movieId") FROM stdin;
1	2	10	20	2020-01-09 16:54:48.589255	2020-01-09 16:54:48.589255	2	1
2	3	15	45	2020-01-09 16:54:48.589255	2020-01-09 16:54:48.589255	2	2
11	5	10	50	2020-01-09 18:48:44.625963	2020-01-09 18:48:44.625963	7	4
12	5	15	75	2020-01-09 18:48:44.625963	2020-01-09 18:48:44.625963	7	3
\.


--
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.rol (id, name, "createdAt", "updatedAt") FROM stdin;
1	admin	2020-01-09 09:54:38.7331	2020-01-09 09:54:38.7331
2	client	2020-01-09 09:54:38.7331	2020-01-09 09:54:38.7331
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.tag (id, name, "isActive", "createdAt", "updatedAt") FROM stdin;
1	Science Fiction	t	2020-01-09 11:16:36.773599	2020-01-09 11:16:36.773599
2	Space	t	2020-01-09 11:16:36.773599	2020-01-09 11:16:36.773599
4	Fantastic	t	2020-01-09 11:17:16.990235	2020-01-09 11:17:16.990235
5	Genetic	t	2020-01-09 12:11:51.672397	2020-01-09 12:11:51.672397
3	Action	t	2020-01-09 11:17:16.990235	2020-01-09 12:00:01.47427
\.


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public.token (id, token, "createdAt", "userId") FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: kevin
--

COPY public."user" (id, username, password, "firstName", "lastName", email, "isActive", "createdAt", "updatedAt", "rolId") FROM stdin;
2	kescoto	$2b$10$xmoUjrirGn3qk5pv/de3WOgAXDvyyL6bbJVf.uGXDPkz/lU1vkREi	Kevin	Escoto	kescoto@gmail.com	t	2020-01-09 10:33:55.228334	2020-01-09 10:33:55.228334	1
3	amata	$2b$10$yipjdOiYsDlyPWjGByNYC.jm5WFLWI/Pt99Yzooz0O6tBWcADWm6C	Antonio	Mata	amata@gmail.com	t	2020-01-09 10:36:25.744072	2020-01-09 10:36:25.744072	2
7	pberganza	$2b$10$jNVv96TqAMTBY9ooPYEFzuNFrC0hBblf6bHn0AtphfkbGKdwnjOD6	Pablo	Berganza	pberganza@gmail.com	t	2020-01-09 10:48:13.877889	2020-01-09 10:48:13.877889	1
4	frivas	$2b$10$imT48hf6T5YQG1/oIYugxexHYqxJWxK2xQXpkouRhTso3R1ikZkSy	Francisco	Rivas	frivas@gmail.com	t	2020-01-09 10:40:29.835411	2020-01-09 13:29:44.232152	2
5	lmolina	$2b$10$MyyrJq/eGWgIW4XhT5pVaO0SCds54IUb13CMxmvcMJC0fCPGBCbAa	Luis	Molina Molina	lmolina@outlook.com	f	2020-01-09 10:41:39.004189	2020-01-10 12:35:37.279465	2
1	acoto	$2b$10$tatz5eXWkQPMvmyMjWGfHu8lQjb7kZIgV8I2V/C7BoIJLl2E255QG	Alexis	Coto	acoto@gmail.com	t	2020-01-09 10:17:56.886768	2020-01-10 12:37:28.755309	2
\.


--
-- Name: movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.movie_id_seq', 7, true);


--
-- Name: order_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.order_detail_id_seq', 14, true);


--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.order_id_seq', 7, true);


--
-- Name: rent_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.rent_detail_id_seq', 16, true);


--
-- Name: rent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.rent_id_seq', 9, true);


--
-- Name: rol_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.rol_id_seq', 2, true);


--
-- Name: tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.tag_id_seq', 6, true);


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.token_id_seq', 50, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('public.user_id_seq', 9, true);


--
-- Name: order_detail PK_0afbab1fa98e2fb0be8e74f6b38; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.order_detail
    ADD CONSTRAINT "PK_0afbab1fa98e2fb0be8e74f6b38" PRIMARY KEY (id);


--
-- Name: order PK_1031171c13130102495201e3e20; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT "PK_1031171c13130102495201e3e20" PRIMARY KEY (id);


--
-- Name: rent PK_211f726fd8264e82ff7a2b86ce2; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent
    ADD CONSTRAINT "PK_211f726fd8264e82ff7a2b86ce2" PRIMARY KEY (id);


--
-- Name: rent_detail PK_4e1016403fdfe257dc4a7ae83f2; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent_detail
    ADD CONSTRAINT "PK_4e1016403fdfe257dc4a7ae83f2" PRIMARY KEY (id);


--
-- Name: token PK_82fae97f905930df5d62a702fc9; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT "PK_82fae97f905930df5d62a702fc9" PRIMARY KEY (id);


--
-- Name: tag PK_8e4052373c579afc1471f526760; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT "PK_8e4052373c579afc1471f526760" PRIMARY KEY (id);


--
-- Name: movie_tags_tag PK_a63fb1cc6083d9417e67029dece; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.movie_tags_tag
    ADD CONSTRAINT "PK_a63fb1cc6083d9417e67029dece" PRIMARY KEY ("movieId", "tagId");


--
-- Name: rol PK_c93a22388638fac311781c7f2dd; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT "PK_c93a22388638fac311781c7f2dd" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: movie PK_cb3bb4d61cf764dc035cbedd422; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY (id);


--
-- Name: rol UQ_642b883443b82d52f4ba99589c9; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT "UQ_642b883443b82d52f4ba99589c9" UNIQUE (name);


--
-- Name: tag UQ_6a9775008add570dc3e5a0bab7b; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT "UQ_6a9775008add570dc3e5a0bab7b" UNIQUE (name);


--
-- Name: user UQ_78a916df40e02a9deb1c4b75edb; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE (username);


--
-- Name: movie UQ_a81090ad0ceb645f30f9399c347; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT "UQ_a81090ad0ceb645f30f9399c347" UNIQUE (title);


--
-- Name: token UQ_d9959ee7e17e2293893444ea371; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT "UQ_d9959ee7e17e2293893444ea371" UNIQUE (token);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: IDX_5c229532ab9c842d9f712c44a4; Type: INDEX; Schema: public; Owner: kevin
--

CREATE INDEX "IDX_5c229532ab9c842d9f712c44a4" ON public.movie_tags_tag USING btree ("movieId");


--
-- Name: IDX_7f5d867068b30d8263854b3e98; Type: INDEX; Schema: public; Owner: kevin
--

CREATE INDEX "IDX_7f5d867068b30d8263854b3e98" ON public.movie_tags_tag USING btree ("tagId");


--
-- Name: order_detail FK_053665fe719e0a2132b493aed1f; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.order_detail
    ADD CONSTRAINT "FK_053665fe719e0a2132b493aed1f" FOREIGN KEY ("movieId") REFERENCES public.movie(id);


--
-- Name: rent_detail FK_169db08fa5132fa94005d5c2503; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent_detail
    ADD CONSTRAINT "FK_169db08fa5132fa94005d5c2503" FOREIGN KEY ("movieId") REFERENCES public.movie(id);


--
-- Name: rent_detail FK_28adeabcfdeaefa820535abf437; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent_detail
    ADD CONSTRAINT "FK_28adeabcfdeaefa820535abf437" FOREIGN KEY ("rentId") REFERENCES public.rent(id);


--
-- Name: rent FK_49296d11229074f058b7274ae2e; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.rent
    ADD CONSTRAINT "FK_49296d11229074f058b7274ae2e" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: movie_tags_tag FK_5c229532ab9c842d9f712c44a4d; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.movie_tags_tag
    ADD CONSTRAINT "FK_5c229532ab9c842d9f712c44a4d" FOREIGN KEY ("movieId") REFERENCES public.movie(id) ON DELETE CASCADE;


--
-- Name: movie_tags_tag FK_7f5d867068b30d8263854b3e98d; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.movie_tags_tag
    ADD CONSTRAINT "FK_7f5d867068b30d8263854b3e98d" FOREIGN KEY ("tagId") REFERENCES public.tag(id) ON DELETE CASCADE;


--
-- Name: order_detail FK_88850b85b38a8a2ded17a1f5369; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.order_detail
    ADD CONSTRAINT "FK_88850b85b38a8a2ded17a1f5369" FOREIGN KEY ("orderId") REFERENCES public."order"(id);


--
-- Name: token FK_94f168faad896c0786646fa3d4a; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT "FK_94f168faad896c0786646fa3d4a" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: order FK_caabe91507b3379c7ba73637b84; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."order"
    ADD CONSTRAINT "FK_caabe91507b3379c7ba73637b84" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: user FK_f66058a8f024b32ce70e0d6a929; Type: FK CONSTRAINT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "FK_f66058a8f024b32ce70e0d6a929" FOREIGN KEY ("rolId") REFERENCES public.rol(id);


--
-- PostgreSQL database dump complete
--

