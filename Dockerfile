FROM node:12.14

# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
COPY . .

# install dependencies
RUN npm i

# Compile the project
RUN npm run build
