# Movie Rental API v2 - Week 9 - Applaudo Studios

## Prerrequisites

1. Install PostgreSQL v11.x
2. Install NodeJS v12.x and npm v6.x

## Installation

1. Clone the repository.
   > \$ git clone https://github.com/KevinARE29/MovieRentalAPI-v2.git
2. Install dependencies.
   > \$ npm i
3. Create the database MovieRentalDB on PostgreSQL.
4. Restore the dump on database MovieRentalDB:

   > \$ psql MovieRentalDB < scripts/MovieRentalDB-backup.sql

5. Configure your .env file, check the .env.example file on root folder to see the required variable names.

<center>

| Environment Variable Key            | Environment Variable Value                                                   |
| ----------------------------------- | ---------------------------------------------------------------------------- |
| JWT_SECRET                          | [Your JWT SECRET]                                                            |
| JWT_PASSWORD_RECOVERY               | [Your JWT_PASSWORD_RECOVERY]                                                 |
| SENDGRID_API_KEY                    | [Your SENDGRID_API_KEY]                                                      |
| ORDER_SENDGRID_TEMPLATE_ID          | [Your ORDER_SENDGRID_TEMPLATE_ID]                                            |
| RENT_SENDGRID_TEMPLATE_ID           | [Your RENT_SENDGRID_TEMPLATE_ID]                                             |
| RESET_PASSWORD_SENDGRID_TEMPLATE_ID | [Your RESET_PASSWORD_SENDGRID_TEMPLATE_ID]                                   |
| TYPEORM_CONNECTION                  | postgres                                                                     |
| TYPEORM_HOST                        | ['db' for start with docker-compose or 'localhost' for start without docker] |
| TYPEORM_USERNAME                    | [YOUR DB USER-USERNAME]                                                      |
| TYPEORM_PASSWORD                    | [YOUR DB USER-PASS]                                                          |
| TYPEORM_DATABASE                    | MovieRentalDB                                                                |
| TYPEORM_PORT                        | 5432                                                                         |
| TYPEORM_SYNCHRONIZE                 | true                                                                         |
| TYPEORM_ENTITIES                    | entity/_.js,dist/\*\*/entities/_.js                                          |
| PORT                                | 3000                                                                         |

</center>

6. If you want to start with docker compose, configure the DB credentials that you want to use in the docker-compose.yml file.

<center>

| DB Param Name     | BD Param Value          |
| ----------------- | ----------------------- |
| POSTGRES_USER     | [YOUR DB USER-USERNAME] |
| POSTGRES_PASSWORD | [YOUR DB USER-PASS]     |

</center>

## Usage

1. Start the server using npm:
   > \$ npm run start
2. You can test the API with the admin user: kescoto/SecretPass.
3. You can import the Postman collection in your local in order to test the endpoints:
   scripts/MovieRentalAPI.postman_collection.json or also you can user Swagger Documentation.

## Testing

1. Execute the test script:

   > \$ npm run test

2. Execute the test:cov script:
   > \$ npm run test:cov

## API Documentation

The API Swagger Documentation is available through the /api path.

## Heroku Deployed API.

https://stark-cove-16192.herokuapp.com/api/
