import { IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TagDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;
}
