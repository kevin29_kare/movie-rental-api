import { Test, TestingModule } from '@nestjs/testing';
import { TagRepository } from './tags.repository';

const mockTag = {
  id: 1,
  name: 'Action',
  isActive: true,
};

const mockTags = ['Space', 'Action'];

const mockFindOrCreatedTags = [
  {
    id: 1,
    name: 'Action',
    isActive: true,
  },
  {
    id: 2,
    name: 'Space',
    isActive: true,
  },
];
describe('TagsRepository', () => {
  let tagRepository: TagRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TagRepository],
    }).compile();

    tagRepository = module.get<TagRepository>(TagRepository);
    tagRepository.findOne = jest.fn().mockResolvedValue(mockTag);
    tagRepository.find = jest.fn().mockResolvedValue([]);
    tagRepository.save = jest.fn().mockResolvedValue(mockFindOrCreatedTags);
  });
  it('Should be defined', () => {
    expect(tagRepository).toBeDefined();
  });

  describe('Find Tag by id', () => {
    it('Should Find a tag with id 1', async () => {
      const result = await tagRepository.findById(1);
      expect(result).toEqual(mockTag);
    });
  });

  describe('FindOrCreateTags', () => {
    it('Should Find or create tags', async () => {
      const result = await tagRepository.findOrCreateTags(mockTags);
      expect(result).toEqual(mockFindOrCreatedTags);
    });
  });
});
