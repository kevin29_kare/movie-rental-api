import { IsNotEmpty, ValidateNested, ArrayNotEmpty, IsDateString } from 'class-validator';
import { CreateRentDetailDto } from './create-rent-detail.dto';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreateRentDto {
  @IsNotEmpty()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => CreateRentDetailDto)
  @ApiProperty({ type: [CreateRentDetailDto] })
  rentDetails: CreateRentDetailDto[];

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty()
  returnDate: Date;
}
