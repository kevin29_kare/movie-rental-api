export interface RentDetailI {
  unitPrice: number;
  subTotal: number;
  movieId: number;
  movieTitle: string;
  quantity: number;
}
