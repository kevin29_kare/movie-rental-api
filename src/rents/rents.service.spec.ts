import { Test, TestingModule } from '@nestjs/testing';
import { Repository, UpdateResult } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RentsService } from './rents.service';
import { MoviesService } from '../movies/movies.service';
import { RentDetail } from './entities/rent-details.entity';
import { RentRepository } from './repositories/rents.repository';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { User } from 'src/users/entities/users.entity';
import { Rent } from './entities/rents.entity';
import { CreateRentDto } from './dto/create-rent.dto';

const mockCreateRentDetailDto = [
  {
    quantity: 2,
    movieId: 1,
  },
  {
    quantity: 5,
    movieId: 2,
  },
];

const mockCreateRentDto = {
  returnDate: new Date('2030-02-10T00:17:57.496Z'),
  rentDetails: mockCreateRentDetailDto,
};

const mockMovieFewStock = {
  id: 1,
  stock: 4,
};

const mockMovieEnoughStock = {
  id: 1,
  stock: 10,
};

const mockOrderDuplicatedValues = [...mockCreateRentDetailDto, { quantity: 1, movieId: 2 }];

const mockUser = {
  id: 1,
} as User;

const mockRent = {
  id: 1,
  returned: false,
  returnDate: new Date('2030-02-10T00:17:57.496Z'),
  rentDetails: new Array<unknown>(),
};

const mockRepository = () => ({
  findOne: jest.fn(),
  save: jest.fn(),
  update: jest.fn().mockResolvedValue(UpdateResult),
});

const mockMoviesService = () => ({
  getMovie: jest.fn(),
  saveMovie: jest.fn(),
});

describe('Rents Service', () => {
  let rentsService: RentsService;
  let moviesService: MoviesService;
  let rentRepository: RentRepository;
  let rentDetailRepository: Repository<RentDetail>;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RentsService,
        { provide: MoviesService, useFactory: mockMoviesService },
        { provide: RentRepository, useFactory: mockRepository },
        { provide: getRepositoryToken(RentDetail), useFactory: mockRepository },
      ],
    }).compile();

    rentsService = module.get(RentsService);
    moviesService = module.get(MoviesService);
    rentRepository = module.get(RentRepository);
    rentDetailRepository = module.get(getRepositoryToken(RentDetail));
  });
  it('Should be defined', () => {
    expect(rentsService).toBeDefined();
    expect(moviesService).toBeDefined();
    expect(rentRepository).toBeDefined();
    expect(rentDetailRepository).toBeDefined();
  });

  describe('Get rent by id and user', () => {
    it('Should get a rent by id and user', async () => {
      (rentRepository.findOne as jest.Mock).mockResolvedValue(mockRent);
      const result = await rentsService.getRentByIdAndUser(1, mockUser);
      expect(result).toEqual(mockRent);
    });
    it('Should throw for rent not found', async () => {
      (rentRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(rentsService.getRentByIdAndUser(1, mockUser)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('Return Rented Movie', () => {
    it('Should return an rented movie', async () => {
      const result = await rentsService.returnRentedMovie(mockRent as Rent);
      expect(result).toEqual(UpdateResult);
    });
  });

  describe('Check Duplicated Movies', () => {
    it('Should check for duplicated movies', () => {
      const result = rentsService.containsDuplicatedMovies(mockOrderDuplicatedValues);
      expect(result).toEqual(true);
    });
  });

  describe('Check Stock Availability Movies', () => {
    it('Should check stock availability of movies', async () => {
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieEnoughStock);
      const result = await rentsService.checkStockAvailability(mockCreateRentDetailDto);
      expect(result).toEqual(undefined);
    });
    it('Should throw error for not sufficient stock', async () => {
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieFewStock);
      expect(rentsService.checkStockAvailability(mockCreateRentDetailDto)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('Adjust Stock', () => {
    it('Should adjust the stock of a movie', async () => {
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieEnoughStock);
      await rentsService.adjustStock(mockCreateRentDetailDto, false);
      expect(moviesService.saveMovie).toHaveBeenCalledTimes(2);
    });
  });

  describe('Add Rents to user', () => {
    it('Should add rents to user', async () => {
      (rentRepository.save as jest.Mock).mockResolvedValue(mockRent);
      (rentDetailRepository.save as jest.Mock).mockResolvedValue(new Array<RentDetail>());
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieEnoughStock);
      (moviesService.saveMovie as jest.Mock).mockResolvedValue(mockMovieEnoughStock);
      const result = await rentsService.addRentToUser(mockUser, mockRent as CreateRentDto);
      expect(result).toEqual(mockRent);
    });
    it('Should throw error for not sufficient stock', async () => {
      (rentRepository.save as jest.Mock).mockResolvedValue(mockRent);
      (rentDetailRepository.save as jest.Mock).mockResolvedValue(new Array<RentDetail>());
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieFewStock);
      expect(rentsService.addRentToUser(mockUser, mockCreateRentDto)).rejects.toThrowError(BadRequestException);
    });
  });
});
