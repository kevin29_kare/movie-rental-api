import { Exclude } from 'class-transformer';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';
import { User } from '../../users/entities/users.entity';
import { RentDetail } from '../entities/rent-details.entity';

export class RentSerializer {
  @ApiProperty()
  id: number;

  @ApiProperty()
  total: number;

  @ApiProperty()
  returnDate: Date;

  @ApiProperty()
  returned: boolean;

  @Exclude()
  @ApiHideProperty()
  @ApiProperty()
  user: User;

  @ApiProperty()
  rentDetails: RentDetail[];

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  constructor(partial: Partial<RentSerializer>) {
    Object.assign(this, partial);
  }
}
