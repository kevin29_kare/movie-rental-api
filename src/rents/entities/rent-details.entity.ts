/* istanbul ignore file */
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Check, ManyToOne } from 'typeorm';
import { IsInt, IsPositive } from 'class-validator';
import { Rent } from './rents.entity';
import { Movie } from '../../movies/entities/movies.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
@Check(`"unitPrice" >= 0`)
@Check(`"subTotal" >= 0`)
export class RentDetail {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Rent,
    rent => rent.rentDetails,
    { nullable: false },
  )
  @ApiHideProperty()
  rent: Rent;

  @ManyToOne(
    () => Movie,
    movie => movie.orderDetails,
    {
      eager: true,
      nullable: false,
    },
  )
  @ApiHideProperty()
  movie: Movie;

  @Column()
  @IsInt()
  @IsPositive()
  @ApiProperty()
  quantity: number;

  @Column()
  @ApiProperty()
  unitPrice: number;

  @Column()
  @ApiProperty()
  subTotal: number;

  @CreateDateColumn()
  @ApiProperty()
  createdAt: Date;

  @UpdateDateColumn()
  @ApiProperty()
  updatedAt: Date;
}
