/* istanbul ignore file */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Check,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { User } from '../../users/entities/users.entity';
import { RentDetail } from './rent-details.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
@Check(`"total" >= 0`)
export class Rent {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => RentDetail,
    rentDetail => rentDetail.rent,
  )
  @ApiProperty({ type: [RentDetail] })
  rentDetails: RentDetail[];

  @ManyToOne(
    () => User,
    user => user.rents,
    { nullable: false },
  )
  @ApiHideProperty()
  user: User;

  @Column()
  @ApiProperty()
  total: number;

  @Column()
  @ApiProperty()
  returnDate: Date;

  @Column({ default: false })
  @ApiProperty()
  returned: boolean;

  @CreateDateColumn()
  @ApiProperty()
  createdAt: Date;

  @UpdateDateColumn()
  @ApiProperty()
  updatedAt: Date;
}
