import { User } from 'src/users/entities/users.entity';
import { Order } from 'src/orders/entities/orders.entity';
import { Rent } from 'src/rents/entities/rents.entity';

export interface BillingTemplateI {
  user: User;
  billingData: Order | Rent;
  billingDate: string;
  returnDate?: string;
}
