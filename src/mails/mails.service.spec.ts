import { Test, TestingModule } from '@nestjs/testing';
import { MailsService } from './mails.service';
import { ConfigService } from '@nestjs/config';
import { send } from '@sendgrid/mail';
import { User } from 'src/users/entities/users.entity';
import { Order } from 'src/orders/entities/orders.entity';
import { Rent } from 'src/rents/entities/rents.entity';

jest.mock('@sendgrid/mail');

const mockUser = {
  id: 1,
  email: 'fake@email.com',
} as User;

const mockOrder = {
  id: 1,
} as Order;

const mockRent = {
  id: 1,
} as Rent;

describe('TagsService', () => {
  let mailsService: MailsService;
  let configService: ConfigService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MailsService, ConfigService],
    }).compile();

    mailsService = module.get(MailsService);
    configService = module.get(ConfigService);
  });
  it('Should be defined', () => {
    expect(mailsService).toBeDefined();
    expect(configService).toBeDefined();
  });

  describe('Send Order Billing Email', () => {
    it('Should send a order billing email', async () => {
      expect(send).not.toHaveBeenCalled();
      mailsService.sendBillingMail(mockUser, mockOrder, 'order');
      expect(send).toHaveBeenCalledTimes(1);
    });
  });

  describe('Send Rent Billing Email', () => {
    it('Should send a rent billing email', async () => {
      mailsService.sendBillingMail(mockUser, mockRent, 'rent');
      expect(send).toHaveBeenCalledTimes(2);
    });
  });

  describe('Send Reset Password Email', () => {
    it('Should send a reset password email', async () => {
      mailsService.sendResetPasswordEmail(mockUser, 'fake.url');
      expect(send).toHaveBeenCalledTimes(3);
    });
  });
});
