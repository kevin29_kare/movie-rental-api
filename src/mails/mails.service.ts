import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { setApiKey, send } from '@sendgrid/mail';
import { Rent } from 'src/rents/entities/rents.entity';
import { Order } from 'src/orders/entities/orders.entity';
import { User } from 'src/users/entities/users.entity';
import { BillingTemplateI } from './interfaces/billing-template.interface';

@Injectable()
export class MailsService {
  private readonly dateFormat = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  constructor(private readonly configService: ConfigService) {
    setApiKey(this.configService.get('SENDGRID_API_KEY') || '');
  }

  sendResetPasswordEmail(user: User, url: string): void {
    const msg = {
      to: user.email,
      from: 'mailer@movie-rental-api.com',
      templateId: this.configService.get('RESET_PASSWORD_SENDGRID_TEMPLATE_ID'),
      dynamicTemplateData: {
        user: {
          firstName: user.firstName,
          lastName: user.lastName,
        },
        url,
      },
    };
    try {
      send(msg);
    } catch (err) {
      // console.error(`Error while sending the email to ${user.email}`);
    }
  }

  sendBillingMail(user: User, billingData: Rent | Order, type: 'rent' | 'order'): void {
    let templateId: string | undefined;
    let dynamicTemplateData: BillingTemplateI = {
      user,
      billingData,
      billingDate: new Date(billingData.createdAt).toLocaleDateString('en-US', this.dateFormat),
    };
    if (type === 'order') {
      templateId = this.configService.get('ORDER_SENDGRID_TEMPLATE_ID');
    } else {
      templateId = this.configService.get('RENT_SENDGRID_TEMPLATE_ID');
      dynamicTemplateData = {
        ...dynamicTemplateData,
        returnDate: new Date((billingData as Rent).returnDate).toLocaleDateString('en-US', this.dateFormat),
      };
    }
    const msg = {
      to: user.email,
      from: 'mailer@movie-rental-api.com',
      templateId,
      dynamicTemplateData,
    };
    try {
      send(msg);
    } catch (err) {
      // console.error(`Error while sending the email to ${user.email}`);
    }
  }
}
