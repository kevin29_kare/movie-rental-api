export interface ResetPasswordTokenI {
  sub: number;
  email: string;
  iat: number;
  exp: number;
}
