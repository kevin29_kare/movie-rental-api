import { Controller, Post, UseGuards, HttpCode, Body, Req, Query } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { AccessToken } from './interfaces/access-token.interface';
import { UserParam } from '../auth/decorators/user.decorator';
import { AuthenticatedUserI } from '../users/interfaces/user.interface';
import { UsersService } from '../users/users.service';
import { TokenParam } from './decorators/token.decorator';
import { DeleteResult } from 'typeorm';
import { ApiBody, ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { LoginDto } from './dto/login.dto';
import { PasswordRecoveryDto } from './dto/password-recovery.dto';
import * as jwt from 'jsonwebtoken';
import { Request } from 'express';
import { MailsService } from '../mails/mails.service';
import { PasswordSetDto } from './dto/password-set.dto';
import { ResetPasswordGuard } from './guards/reset-password.guard';
import { ResetPasswordTokenI } from './interfaces/reset-password-token.interface';
import { ConfigService } from '@nestjs/config';

@Controller('auth')
@ApiTags('Auth Endpoints')
export class AuthController {
  private readonly JwtPasswordRecovery: string;
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly mailsService: MailsService,
    private readonly configService: ConfigService,
  ) {
    this.JwtPasswordRecovery = this.configService.get('JWT_PASSWORD_RECOVERY') || 'testKey';
  }

  @UseGuards(AuthGuard('local'))
  @Post('signIn')
  @ApiBody({ type: LoginDto })
  async login(@UserParam() user: AuthenticatedUserI): Promise<AccessToken> {
    const validUser = await this.usersService.getUser(user.id);
    const token = this.authService.getTokenForUser(user);
    await this.authService.saveToken(validUser, token.accessToken);
    return token;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('signOut')
  @ApiBearerAuth()
  @HttpCode(200)
  async logout(@TokenParam() bearerToken: string): Promise<DeleteResult> {
    const token = await this.authService.getToken(bearerToken);
    return this.authService.deleteToken(token);
  }

  @Post('password/recovery')
  @HttpCode(200)
  async passwordRecovery(@Req() req: Request, @Body() passwordRecoveryDto: PasswordRecoveryDto): Promise<void> {
    const user = await this.usersService.findByEmail(passwordRecoveryDto.email);
    const token = jwt.sign({ sub: user.id, email: user.email }, this.JwtPasswordRecovery, { expiresIn: '1h' });
    const url = req.headers.host + '/password/set?token=' + token;
    await this.authService.createOrReplaceResetPasswordToken(user, token);
    this.mailsService.sendResetPasswordEmail(user, url);
  }

  @UseGuards(ResetPasswordGuard)
  @Post('password/set')
  @HttpCode(200)
  async passwordSet(@Query('token') passwordResetToken: string, @Body() passwordSetDto: PasswordSetDto): Promise<void> {
    const user = jwt.verify(passwordResetToken, this.JwtPasswordRecovery) as ResetPasswordTokenI;
    await this.usersService.changePassword(user.sub, passwordSetDto.password);
    const token = await this.authService.getResetPasswordToken(passwordResetToken);
    this.authService.deleteResetPasswordToken(token);
  }
}
