import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { Token } from './entities/tokens.entity';
import { DeleteResult, Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { ResetPasswordToken } from './entities/reset-password-token.entity';
import { Rol } from './entities/roles.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UnauthorizedException, NotFoundException } from '@nestjs/common';
import { User } from 'src/users/entities/users.entity';

const mockToken = {
  token: 'fakeToken',
};

const mockResetPasswordToken = {
  resetPasswordToken: 'fakeToken',
};

const mockUser = {
  id: 1,
  username: 'kescoto',
  password: 'SecretPass',
};

const mockRepository = () => ({
  findOne: jest.fn(),
  delete: jest.fn().mockResolvedValue(DeleteResult),
  save: jest.fn(),
});

const mockUsersService = () => ({
  findByUserName: jest.fn(),
  compareHash: jest.fn(),
});

const mockJwtService = () => ({
  sign: jest.fn(),
});

describe('Movies Service', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let jwtService: JwtService;
  let tokenRepository: Repository<Token>;
  let resetPasswordTokenRepository: Repository<ResetPasswordToken>;
  let rolRepository: Repository<Rol>;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        { provide: UsersService, useFactory: mockUsersService },
        { provide: JwtService, useFactory: mockJwtService },
        { provide: getRepositoryToken(Token), useFactory: mockRepository },
        { provide: getRepositoryToken(ResetPasswordToken), useFactory: mockRepository },
        { provide: getRepositoryToken(Rol), useFactory: mockRepository },
      ],
    }).compile();

    authService = module.get(AuthService);
    usersService = module.get(UsersService);
    jwtService = module.get(JwtService);
    tokenRepository = module.get(getRepositoryToken(Token));
    resetPasswordTokenRepository = module.get(getRepositoryToken(ResetPasswordToken));
    rolRepository = module.get(getRepositoryToken(Rol));
  });
  it('Should be defined', () => {
    expect(authService).toBeDefined();
    expect(usersService).toBeDefined();
    expect(jwtService).toBeDefined();
    expect(tokenRepository).toBeDefined();
    expect(resetPasswordTokenRepository).toBeDefined();
    expect(rolRepository).toBeDefined();
  });

  describe('Get Token', () => {
    it('Should Get a token', async () => {
      (tokenRepository.findOne as jest.Mock).mockResolvedValue(mockToken);
      expect(tokenRepository.findOne).not.toHaveBeenCalled();
      const result = await authService.getToken('fakeToken');
      expect(result).toEqual(mockToken);
      expect(tokenRepository.findOne).toHaveBeenCalled();
    });
    it('Should throw an error when the token does not exist', async () => {
      (tokenRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(authService.getToken('inexistentToken')).rejects.toThrowError(UnauthorizedException);
    });
  });

  describe('Get Reset Password Token', () => {
    it('Should Get a reset password token', async () => {
      (resetPasswordTokenRepository.findOne as jest.Mock).mockResolvedValue(mockResetPasswordToken);
      expect(resetPasswordTokenRepository.findOne).not.toHaveBeenCalled();
      const result = await authService.getResetPasswordToken('fakeToken');
      expect(result).toEqual(mockResetPasswordToken);
      expect(resetPasswordTokenRepository.findOne).toHaveBeenCalled();
    });
    it('Should throw an error when the token does not exist', async () => {
      (resetPasswordTokenRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(authService.getResetPasswordToken('inexistentToken')).rejects.toThrowError(UnauthorizedException);
    });
  });

  describe('Get Role by RoleName', () => {
    it('Should Get a role by it name', async () => {
      (rolRepository.findOne as jest.Mock).mockResolvedValue('AdminRole');
      expect(rolRepository.findOne).not.toHaveBeenCalled();
      const result = await authService.getRolByName('admin');
      expect(result).toEqual('AdminRole');
      expect(rolRepository.findOne).toHaveBeenCalled();
    });
    it('Should throw an error when the token does not exist', async () => {
      (rolRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(authService.getRolByName('inexistentToken')).rejects.toThrowError(NotFoundException);
    });
  });

  describe('Validate User', () => {
    it('Should validate the credentials of the user', async () => {
      (usersService.findByUserName as jest.Mock).mockResolvedValue(mockUser);
      (usersService.compareHash as jest.Mock).mockReturnValue(true);
      const result = await authService.validateUser('kescoto', 'SecretPass');
      expect(result).toEqual({ id: mockUser.id, username: mockUser.username });
    });
    it('Should return null for credential not valid', async () => {
      (usersService.findByUserName as jest.Mock).mockResolvedValue(mockUser);
      (usersService.compareHash as jest.Mock).mockReturnValue(false);
      const result = await authService.validateUser('kescoto', 'SecretPass');
      expect(result).toEqual(null);
    });
  });

  describe('Create or Replace Reset Password Token', () => {
    it('Should Get a reset password token', async () => {
      (resetPasswordTokenRepository.findOne as jest.Mock).mockResolvedValue(mockResetPasswordToken);
      (resetPasswordTokenRepository.save as jest.Mock).mockResolvedValue(mockResetPasswordToken);
      expect(resetPasswordTokenRepository.findOne).not.toHaveBeenCalled();
      const result = await authService.createOrReplaceResetPasswordToken(mockUser as User, 'fakeToken');
      expect(resetPasswordTokenRepository.findOne).toHaveBeenCalled();
      expect(result).toEqual(mockResetPasswordToken);
    });
  });
});
