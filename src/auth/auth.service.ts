import { Injectable, NotFoundException, Inject, forwardRef, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { AuthenticatedUserI } from '../users/interfaces/user.interface';
import { JwtService } from '@nestjs/jwt';
import { AccessToken } from './interfaces/access-token.interface';
import { Rol } from './entities/roles.entity';
import { Token } from './entities/tokens.entity';
import { User } from '../users/entities/users.entity';
import { DeleteResult, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ResetPasswordToken } from './entities/reset-password-token.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(Rol)
    private readonly rolRepository: Repository<Rol>,
    @InjectRepository(ResetPasswordToken)
    private readonly resetPasswordRepository: Repository<ResetPasswordToken>,
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
    @Inject(forwardRef(() => UsersService))
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<AuthenticatedUserI | null> {
    const user = await this.usersService.findByUserName(username);
    if (user && this.usersService.compareHash(password, user.password)) {
      const authenticatedUser = {
        id: user.id,
        username: user.username,
      };
      return authenticatedUser;
    }
    return null;
  }

  getTokenForUser(user: AuthenticatedUserI): AccessToken {
    return {
      accessToken: this.jwtService.sign(user),
    };
  }

  async getToken(token: string): Promise<Token> {
    const jwtToken = await this.tokenRepository.findOne({ where: { token } });
    if (!jwtToken) {
      throw new UnauthorizedException();
    }
    return jwtToken;
  }

  saveToken(user: User, token: string): Promise<Token> {
    return this.tokenRepository.save({ token, user });
  }

  deleteToken(token: Token): Promise<DeleteResult> {
    return this.tokenRepository.delete(token.id);
  }

  async getResetPasswordToken(resetPasswordToken: string): Promise<ResetPasswordToken> {
    const jwtToken = await this.resetPasswordRepository.findOne({ where: { resetPasswordToken } });
    if (!jwtToken) {
      throw new UnauthorizedException();
    }
    return jwtToken;
  }

  async createOrReplaceResetPasswordToken(user: User, resetPasswordToken: string): Promise<ResetPasswordToken> {
    const token = await this.resetPasswordRepository.findOne({ where: { user } });
    if (token) {
      token.resetPasswordToken = resetPasswordToken;
      return this.resetPasswordRepository.save(token);
    } else {
      return this.resetPasswordRepository.save({ resetPasswordToken, user });
    }
  }

  deleteResetPasswordToken(resetPasswordToken: ResetPasswordToken): Promise<DeleteResult> {
    return this.resetPasswordRepository.delete(resetPasswordToken.id);
  }

  async getRolByName(name: string): Promise<Rol> {
    const rol = await this.rolRepository.findOne({ where: { name } });
    if (!rol) {
      throw new NotFoundException(`Rol with name ${name} not found`);
    }
    return rol;
  }
}
