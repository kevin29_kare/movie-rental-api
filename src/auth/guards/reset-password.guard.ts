import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { ResetPasswordToken } from '../entities/reset-password-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request } from 'express';

@Injectable()
export class ResetPasswordGuard implements CanActivate {
  constructor(
    @InjectRepository(ResetPasswordToken)
    private readonly resetPasswordRepository: Repository<ResetPasswordToken>,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    const resetPasswordToken = req.query.token;
    const token = await this.resetPasswordRepository.findOne({ where: { resetPasswordToken } });
    if (!token) {
      return false;
    }
    return true;
  }
}
