import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Token } from '../entities/tokens.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class SessionGuard implements CanActivate {
  constructor(
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest();
    const authHeader = req.headers.authorization;
    const token = authHeader.split(' ')[1];
    const jwtToken = await this.tokenRepository.findOne({ where: { token } });
    if (!jwtToken) {
      return false;
    }
    return true;
  }
}
