import { IsNotEmpty, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PasswordRecoveryDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;
}
