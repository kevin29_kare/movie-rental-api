import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class PasswordSetDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  password: string;
}
