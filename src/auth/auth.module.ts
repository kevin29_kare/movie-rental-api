import { Module, forwardRef } from '@nestjs/common';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthController } from './auth.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rol } from './entities/roles.entity';
import { Token } from './entities/tokens.entity';
import { MailsModule } from '../mails/mails.module';
import { ResetPasswordToken } from './entities/reset-password-token.entity';

@Module({
  imports: [
    forwardRef(() => UsersModule),
    PassportModule,
    MailsModule,
    TypeOrmModule.forFeature([Rol, Token, ResetPasswordToken]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET') || 'testKey',
        signOptions: { expiresIn: '24h' },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [TypeOrmModule, AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
