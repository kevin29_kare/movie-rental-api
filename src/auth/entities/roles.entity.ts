/* istanbul ignore file */
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { User } from '../../users/entities/users.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
export class Rol {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => User,
    user => user.rol,
  )
  @ApiHideProperty()
  users: User[];

  @ApiProperty()
  @Column({ length: 128, unique: true })
  name: string;

  @ApiProperty()
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updatedAt: Date;
}
