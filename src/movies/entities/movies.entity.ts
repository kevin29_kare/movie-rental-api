/* istanbul ignore file */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  Check,
  OneToMany,
} from 'typeorm';
import { Tag } from '../../tags/entities/tags.entity';
import { OrderDetail } from '../../orders/entities/order-details.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
@Check(`"stock" >= 0`)
@Check(`"salePrice" >= 0`)
@Check(`"rentPrice" >= 0`)
@Check(`"likes" >= 0`)
export class Movie {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiHideProperty()
  @OneToMany(
    () => OrderDetail,
    orderDetail => orderDetail.order,
  )
  orderDetails: OrderDetail[];

  @ApiProperty()
  @ManyToMany(
    () => Tag,
    tag => tag.name,
    {
      eager: true,
    },
  )
  @JoinTable()
  tags: Tag[];

  @ApiProperty()
  @Column({ unique: true })
  title: string;

  @ApiProperty()
  @Column()
  description: string;

  @ApiProperty()
  @Column({ nullable: true })
  poster: string;

  @ApiProperty()
  @Column({ type: 'int' })
  stock: number;

  @ApiProperty()
  @Column({ nullable: true })
  trailer: string;

  @ApiProperty()
  @Column()
  salePrice: number;

  @ApiProperty()
  @Column()
  rentPrice: number;

  @ApiProperty()
  @Column({ type: 'int', default: 0 })
  likes: number;

  @ApiProperty()
  @Column({ default: true })
  isActive: boolean;

  @ApiProperty()
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updatedAt: Date;
}
