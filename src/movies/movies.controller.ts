import { Controller, Get, Post, Delete, ParseIntPipe, Param, Body, Put, UseGuards, Query } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { Movie } from './entities/movies.entity';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Roles } from '../auth/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { SessionGuard } from '../auth/guards/session.guard';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { FilterMovieQueryDto } from './dto/filter-movie-query.dto';

@Controller('movies')
@ApiTags('Movies Endpoints')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}
  @Get()
  getMovies(@Query() filterMovieQueryDto: FilterMovieQueryDto): Promise<Movie[]> {
    return this.moviesService.getMovies(filterMovieQueryDto);
  }

  @Get(':id')
  getMovie(@Param('id', new ParseIntPipe()) id: number): Promise<Movie> {
    return this.moviesService.getMovie(id);
  }

  @Post()
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), SessionGuard, RolesGuard)
  @Roles('admin')
  createMovie(@Body() createMovieDto: CreateMovieDto): Promise<Movie> {
    return this.moviesService.createMovie(createMovieDto);
  }

  @Put(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), SessionGuard, RolesGuard)
  @Roles('admin')
  updateMovie(@Param('id', new ParseIntPipe()) id: number, @Body() updateMovieDto: UpdateMovieDto): Promise<Movie> {
    return this.moviesService.updateMovie(id, updateMovieDto);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), SessionGuard, RolesGuard)
  @Roles('admin')
  removeMovie(@Param('id', new ParseIntPipe()) id: number): Promise<Movie> {
    return this.moviesService.removeMovie(id);
  }
}
