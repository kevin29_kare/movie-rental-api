import { IsString, IsNotEmpty, IsOptional, IsInt, IsNumber, IsArray } from 'class-validator';
import { ApiPropertyOptional, ApiProperty } from '@nestjs/swagger';

export class CreateMovieDto {
  @ApiPropertyOptional()
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  tags?: string[];

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  poster?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsInt()
  stock: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  trailer?: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  salePrice: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  rentPrice: number;
}
