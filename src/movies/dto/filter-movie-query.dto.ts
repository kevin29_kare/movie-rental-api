import { IsString, IsOptional, IsNotEmpty, IsIn } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

const sortOptions = ['title', '-title', 'likes', '-likes'];

export class FilterMovieQueryDto {
  @ApiPropertyOptional({ enum: sortOptions })
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @IsIn(sortOptions)
  sort?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  title?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  tags?: string;
}
