import { Test, TestingModule } from '@nestjs/testing';
import { MovieRepository } from './movies.repository';

const mockMovie = {
  id: 1,
  title: 'Star Wars - The rise of Skywalker',
  description: 'Lets meet with Palpatine',
  isActive: true,
};

describe('Movies Repository', () => {
  let movieRepository: MovieRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MovieRepository],
    }).compile();

    movieRepository = module.get<MovieRepository>(MovieRepository);
    movieRepository.findOne = jest.fn().mockResolvedValue(mockMovie);
  });

  it('Should be defined', () => {
    expect(movieRepository).toBeDefined();
  });

  describe('Find Movie by id', () => {
    it('Should Find a movie with id 1', async () => {
      const result = await movieRepository.findById(1);
      expect(result).toEqual(mockMovie);
    });
  });
});
