import { Test, TestingModule } from '@nestjs/testing';
import { TagsService } from '../tags/tags.service';
import { MoviesService } from '../movies/movies.service';
import { MovieRepository } from '../movies/repositories/movies.repository';
import { NotFoundException } from '@nestjs/common';
import { Movie } from './entities/movies.entity';

const mockCreateMovieDto = {
  title: 'Star Wars - The rise of Skywalker',
  description: 'Lets meet with Palpatine',
  stock: 30,
  salePrice: 25,
  rentPrice: 10,
};

const mockMovie = {
  id: 1,
  ...mockCreateMovieDto,
  isActive: true,
};

const mockUpdateMovieDto = {
  description: 'Lets meet with Palpatine and the last order',
  trailer: 'https://www.youtube.com/watch?v=8Qn_spdM5Zg',
  tags: ['action', 'space'],
};

const mockUpdatedMovieDto = {
  ...mockMovie,
  ...mockUpdateMovieDto,
};

const mockDeletedMovie = {
  ...mockMovie,
  isActive: false,
};

const mockMovieRepository = () => ({
  findAll: jest.fn(),
  findById: jest.fn(),
  save: jest.fn().mockResolvedValue(mockMovie),
  createQueryBuilder: jest.fn(() => ({
    leftJoin: jest.fn().mockReturnThis(),
    orderBy: jest.fn().mockReturnThis(),
    andWhere: jest.fn().mockReturnThis(),
    select: jest.fn().mockReturnThis(),
    getMany: jest.fn().mockReturnValue('Filtered movies'),
  })),
});

const mockTagService = () => ({
  findOrCreateTags: jest.fn(),
});

describe('Movies Service', () => {
  let moviesService: MoviesService;
  let tagsService: TagsService;
  let movieRepository: MovieRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviesService,
        { provide: TagsService, useFactory: mockTagService },
        { provide: MovieRepository, useFactory: mockMovieRepository },
      ],
    }).compile();

    moviesService = module.get<MoviesService>(MoviesService);
    tagsService = module.get<TagsService>(TagsService);
    movieRepository = module.get<MovieRepository>(MovieRepository);
  });
  it('Should be defined', () => {
    expect(moviesService).toBeDefined();
    expect(tagsService).toBeDefined();
    expect(movieRepository).toBeDefined();
  });

  describe('Get Movies', () => {
    it('Should Get filtered movies ordered by likes asc', async () => {
      const result = await moviesService.getMovies({ sort: 'likes', title: 'Star Wars', tags: 'action,space' });
      expect(result).toEqual('Filtered movies');
    });
    it('Should Get filtered movies ordered by likes desc', async () => {
      const result = await moviesService.getMovies({ sort: '-likes', title: 'Star Wars', tags: 'action,space' });
      expect(result).toEqual('Filtered movies');
    });
  });

  describe('Get Movie', () => {
    it('Should Get a movie with id 1', async () => {
      (movieRepository.findById as jest.Mock).mockResolvedValue(mockMovie);
      expect(movieRepository.findById).not.toHaveBeenCalled();
      const result = await moviesService.getMovie(1);
      expect(result).toEqual(mockMovie);
      expect(movieRepository.findById).toHaveBeenCalledWith(1);
    });
    it('Should throw an error when the movie does not exist', async () => {
      (movieRepository.findById as jest.Mock).mockResolvedValue(null);
      expect(moviesService.getMovie(0)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('Create Movie', () => {
    it('Should Create a new Movie', async () => {
      const result = await moviesService.createMovie(mockCreateMovieDto);
      expect(result).toEqual(mockMovie);
    });
  });

  describe('Update Movie', () => {
    it('Should Update a new Movie', async () => {
      (movieRepository.findById as jest.Mock).mockResolvedValue(mockMovie);
      (movieRepository.save as jest.Mock).mockResolvedValue(mockUpdatedMovieDto);
      expect(movieRepository.save).not.toHaveBeenCalled();
      const result = await moviesService.updateMovie(1, mockUpdateMovieDto);
      expect(movieRepository.save).toHaveBeenCalled();
      expect(tagsService.findOrCreateTags).toHaveBeenCalled();
      expect(result).toEqual(mockUpdatedMovieDto);
    });
  });

  describe('Remove Movie', () => {
    it('Should Remove a movie', async () => {
      (movieRepository.findById as jest.Mock).mockResolvedValue(mockMovie);
      expect(movieRepository.findById).not.toHaveBeenCalled();
      const result = await moviesService.removeMovie(1);
      expect(movieRepository.findById).toHaveBeenCalled();
      expect(result).toEqual(mockDeletedMovie);
    });
  });

  describe('Save Movie', () => {
    it('Should Save Movie', async () => {
      expect(movieRepository.save).not.toHaveBeenCalled();
      const result = await moviesService.saveMovie(mockMovie as Movie);
      expect(movieRepository.save).toHaveBeenCalled();
      expect(result).toEqual(mockMovie);
    });
  });
});
