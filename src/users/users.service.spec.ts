import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users/users.service';
import { AuthService } from '../auth/auth.service';
import { NotFoundException } from '@nestjs/common';
import { UserRepository } from './repositories/users.repository';
import { UpdateResult } from 'typeorm';

const mockCreateUserDto = {
  username: 'kescoto',
  password: 'SecretPass',
  firstName: 'Kevin',
  lastName: 'Escoto',
  email: 'kescoto@gmail.com',
  rol: 'admin',
};

const mockUpdateUserDto = {
  lastName: 'Rodriguez Escoto',
  email: 'kescoto@applaudostudios.com',
};

const mockUser = {
  id: 1,
  ...mockCreateUserDto,
  isActive: true,
};

const mockUpdatedUserDto = {
  ...mockUser,
  ...mockUpdateUserDto,
};

const mockDeletedUser = {
  ...mockUser,
  isActive: false,
};

const mockUserRepository = () => ({
  find: jest.fn(),
  findOne: jest.fn(),
  findAll: jest.fn(),
  findUserById: jest.fn(),
  findUserByUsername: jest.fn(),
  update: jest.fn().mockResolvedValue(UpdateResult),
  save: jest.fn().mockResolvedValue(mockUser),
});

const mockAuthService = () => ({
  getRolByName: jest.fn(),
});

describe('UsersService', () => {
  let usersService: UsersService;
  let authService: AuthService;
  let userRepository: UserRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: AuthService, useFactory: mockAuthService },
        { provide: UserRepository, useFactory: mockUserRepository },
      ],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    authService = module.get<AuthService>(AuthService);
    userRepository = module.get<UserRepository>(UserRepository);
  });
  it('Should be defined', () => {
    expect(usersService).toBeDefined();
    expect(authService).toBeDefined();
    expect(userRepository).toBeDefined();
  });

  describe('Get Users', () => {
    it('Should Get all users', async () => {
      (userRepository.findAll as jest.Mock).mockResolvedValue('Returns all users');
      const result = await usersService.getUsers();
      expect(result).toEqual('Returns all users');
    });
  });

  describe('Get User by Id', () => {
    it('Should Get a user with id 1', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(mockUser);
      const result = await usersService.getUser(1);
      expect(result).toEqual(mockUser);
    });
    it('Should throw an error when the user does not exist', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(null);
      expect(usersService.getUser(0)).rejects.toThrowError(NotFoundException);
    });
  });

  describe('Get User by Username', () => {
    it('Should Get a user with username kescoto', async () => {
      (userRepository.findUserByUsername as jest.Mock).mockResolvedValue(mockUser);
      const result = await usersService.findByUserName(mockUser.username);
      expect(result).toEqual(mockUser);
    });
  });

  describe('Create User', () => {
    it('Should Create a new User', async () => {
      const result = await usersService.createUser(mockCreateUserDto);
      expect(result).toEqual(mockUser);
    });
  });

  describe('Update User', () => {
    it('Should Update a new User', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(mockUser);
      (userRepository.save as jest.Mock).mockResolvedValue(mockUpdatedUserDto);
      const result = await usersService.updateUser(1, mockUpdateUserDto);
      expect(result).toEqual(mockUpdatedUserDto);
    });
  });

  describe('Remove User', () => {
    it('Should Remove a user', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(mockUser);
      const result = await usersService.removeUser(1);
      expect(result).toEqual(mockDeletedUser);
    });
  });

  describe('Change Password', () => {
    it('Should change a password', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(mockUser);
      const result = await usersService.changePassword(1, 'SecretPass');
      expect(result).toEqual(UpdateResult);
    });
    it('Should throw an error when the user does not exist', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(null);
      expect(usersService.changePassword(0, 'SecretPass')).rejects.toThrowError(NotFoundException);
    });
  });

  describe('Change Role', () => {
    it('Should change a role', async () => {
      (userRepository.findUserById as jest.Mock).mockResolvedValue(mockUser);
      const result = await usersService.changeRol(1, { rol: 'admin' });
      expect(result).toEqual(mockUser);
    });
  });

  describe('Find by email', () => {
    it('Should find a user by email', async () => {
      (userRepository.findOne as jest.Mock).mockResolvedValue(mockUser);
      const result = await usersService.findByEmail('fake@email.com');
      expect(result).toEqual(mockUser);
    });
    it('Should throw an error when the user does not exist', async () => {
      (userRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(usersService.findByEmail('inexistent@email.com')).rejects.toThrowError(NotFoundException);
    });
  });
});
