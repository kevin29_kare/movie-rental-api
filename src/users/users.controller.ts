import { Controller, UseGuards, Param, ParseIntPipe, Body, Put, Get, Post, Delete, Patch } from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateResult } from 'typeorm';
import { ChangePaswordDto } from './dto/change-password.dto';
import { User } from './entities/users.entity';
import { UpdateUserDto } from './dto/update-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { Order } from '../orders/entities/orders.entity';
import { OrdersService } from '../orders/orders.service';
import { CreateOrderDto } from '../orders/dto/create-order.dto';
import { UserSerializer } from './serializers/user.serializer';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../auth/guards/roles.guard';
import { Roles } from '../auth/decorators/roles.decorator';
import { ChangeRolDto } from './dto/change-rol.dto';
import { SessionGuard } from '../auth/guards/session.guard';
import { Rent } from '../rents/entities/rents.entity';
import { RentsService } from '../rents/rents.service';
import { CreateRentDto } from '../rents/dto/create-rent.dto';
import { UserParam } from '../auth/decorators/user.decorator';
import { AuthenticatedUserI } from './interfaces/user.interface';
import { ApiTags, ApiBearerAuth, ApiResponseProperty } from '@nestjs/swagger';
import { MailsService } from '../mails/mails.service';
import { OrderSerializer } from '../orders/serializers/order.serializer';
import { RentSerializer } from '../rents/serializers/rent.serializer';

@UseGuards(AuthGuard('jwt'), SessionGuard, RolesGuard)
@ApiTags('Users Endpoints')
@ApiBearerAuth()
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly ordersService: OrdersService,
    private readonly rentsService: RentsService,
    private readonly mailsService: MailsService,
  ) {}

  @Roles('admin')
  @Get()
  getUsers(): Promise<User[]> {
    return this.usersService.getUsers();
  }

  @Roles('admin')
  @Get(':id')
  getUser(@Param('id', new ParseIntPipe()) id: number): Promise<User> {
    return this.usersService.getUser(id);
  }
  @Roles('admin')
  @Post()
  async createUser(@Body() createUserDto: CreateUserDto): Promise<UserSerializer> {
    return new UserSerializer(await this.usersService.createUser(createUserDto));
  }

  @Roles('admin')
  @Put(':id')
  updateUser(@Param('id', new ParseIntPipe()) id: number, @Body() updateUserDto: UpdateUserDto): Promise<User> {
    return this.usersService.updateUser(id, updateUserDto);
  }

  @Roles('admin')
  @Delete(':id')
  removeUser(@Param('id', new ParseIntPipe()) id: number): Promise<User> {
    return this.usersService.removeUser(id);
  }

  @Roles('admin')
  @ApiResponseProperty({ type: UpdateResult })
  @Patch(':id/changePassword')
  async changePassword(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() changePasswordDto: ChangePaswordDto,
  ): Promise<UpdateResult> {
    return this.usersService.changePassword(id, changePasswordDto.password);
  }

  @Roles('admin')
  @Patch(':id/changeRole')
  changeRol(@Param('id', new ParseIntPipe()) id: number, @Body() changeRolDto: ChangeRolDto): Promise<User> {
    return this.usersService.changeRol(id, changeRolDto);
  }

  @Roles('admin', 'client')
  @Get('me/orders')
  async getOrdersByUser(@UserParam() authUser: AuthenticatedUserI): Promise<Order[]> {
    const user = await this.usersService.getUser(authUser.id);
    return this.ordersService.getOrdersByUserId(user.id);
  }

  @Roles('admin', 'client')
  @Post('me/orders')
  async addOrderToUser(
    @UserParam() authUser: AuthenticatedUserI,
    @Body() createOrderDto: CreateOrderDto,
  ): Promise<OrderSerializer> {
    const user = await this.usersService.getUser(authUser.id);
    const order = await this.ordersService.addOrderToUser(user, createOrderDto);
    this.mailsService.sendBillingMail(user, order, 'order');
    return new OrderSerializer(order);
  }

  @Roles('admin', 'client')
  @Get('me/rents')
  async getRentsByUser(@UserParam() authUser: AuthenticatedUserI): Promise<Rent[]> {
    const user = await this.usersService.getUser(authUser.id);
    return this.rentsService.getRentsByUserId(user.id);
  }

  @Roles('admin', 'client')
  @Post('me/rents')
  async addRentToUser(@UserParam() authUser: AuthenticatedUserI, @Body() createRentDto: CreateRentDto): Promise<Rent> {
    const user = await this.usersService.getUser(authUser.id);
    const rent = await this.rentsService.addRentToUser(user, createRentDto);
    this.mailsService.sendBillingMail(user, rent, 'rent');
    return new RentSerializer(rent);
  }

  @Roles('admin', 'client')
  @Patch('me/rents/:rentId/return')
  async returnRentedMovie(
    @UserParam() authUser: AuthenticatedUserI,
    @Param('rentId', new ParseIntPipe()) rentId: number,
  ): Promise<UpdateResult> {
    const user = await this.usersService.getUser(authUser.id);
    const rent = await this.rentsService.getRentByIdAndUser(rentId, user);
    return this.rentsService.returnRentedMovie(rent);
  }
}
