import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './repositories/users.repository';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { OrdersModule } from '../orders/orders.module';
import { AuthModule } from '../auth/auth.module';
import { RentsModule } from '../rents/rents.module';
import { MailsModule } from '../mails/mails.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    OrdersModule,
    RentsModule,
    forwardRef(() => AuthModule),
    MailsModule,
  ],
  providers: [UsersService],
  controllers: [UsersController],
  exports: [TypeOrmModule, UsersService],
})
export class UsersModule {}
