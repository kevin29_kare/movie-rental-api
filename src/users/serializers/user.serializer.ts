import { Exclude } from 'class-transformer';
import { Rol } from '../../auth/entities/roles.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

export class UserSerializer {
  @ApiProperty()
  id: number;

  @ApiProperty()
  username: string;

  @Exclude()
  @ApiHideProperty()
  password: string;

  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  rol: Rol;

  @ApiProperty()
  isActive: boolean;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  constructor(partial: Partial<UserSerializer>) {
    Object.assign(this, partial);
  }
}
