import { Test, TestingModule } from '@nestjs/testing';
import { UserRepository } from './users.repository';

const mockCreateUserDto = {
  username: 'kescoto',
  password: 'SecretPass',
  firstName: 'Kevin',
  lastName: 'Escoto',
  email: 'kescoto@gmail.com',
  rol: 'admin',
};

const mockUser = {
  id: 1,
  ...mockCreateUserDto,
  isActive: true,
};

describe('UsersRepository', () => {
  let userRepository: UserRepository;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserRepository],
    }).compile();

    userRepository = module.get<UserRepository>(UserRepository);
    userRepository.findAll = jest.fn().mockResolvedValue('Returns all users');
  });
  it('Should be defined', () => {
    expect(userRepository).toBeDefined();
  });

  describe('Find Users', () => {
    it('Should Find all users', async () => {
      const result = await userRepository.findAll();
      expect(result).toEqual('Returns all users');
    });
  });

  describe('Find User by id', () => {
    beforeEach(() => {
      userRepository.findOne = jest.fn().mockResolvedValue(mockUser);
    });
    it('Should Find a user with id 1', async () => {
      const result = await userRepository.findUserById(1);
      expect(result).toEqual(mockUser);
    });
  });

  describe('Find User by username', () => {
    beforeEach(() => {
      userRepository.findOne = jest.fn().mockResolvedValue(mockUser);
    });
    it('Should Find a user with username: kescoto', async () => {
      const result = await userRepository.findUserByUsername('kescoto');
      expect(result).toEqual(mockUser);
    });
  });

  describe('Find User role', () => {
    beforeEach(() => {
      userRepository.findOne = jest.fn().mockResolvedValue(mockUser);
    });
    it('Should Find a user role', async () => {
      const result = await userRepository.findUserRole(1);
      expect(result).toEqual(mockUser);
    });
  });
});
