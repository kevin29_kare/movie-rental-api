/* istanbul ignore file */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Order } from '../../orders/entities/orders.entity';
import { Rol } from '../../auth/entities/roles.entity';
import { Token } from '../../auth/entities/tokens.entity';
import { Rent } from '../../rents/entities/rents.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
export class User {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  readonly id: number;

  @OneToMany(
    () => Token,
    token => token.user,
  )
  @ApiHideProperty()
  tokens: Token[];

  @ApiProperty()
  @ManyToOne(
    () => Rol,
    rol => rol.users,
    { nullable: false, eager: true },
  )
  rol: Rol;

  @OneToMany(
    () => Order,
    order => order.user,
  )
  @ApiHideProperty()
  orders: Order[];

  @OneToMany(
    () => Rent,
    rent => rent.user,
  )
  @ApiHideProperty()
  rents: Rent[];

  @ApiProperty()
  @Column({ length: 128, unique: true })
  username: string;

  @ApiHideProperty()
  @Column({ length: 60 })
  password: string;

  @ApiProperty()
  @Column()
  firstName: string;

  @ApiProperty()
  @Column()
  lastName: string;

  @ApiProperty()
  @Column({ length: 128, unique: true })
  email: string;

  @ApiProperty()
  @Column({ default: true })
  isActive: boolean;

  @ApiProperty()
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updatedAt: Date;
}
