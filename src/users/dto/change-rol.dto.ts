import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ChangeRolDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly rol: string;
}
