import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ChangePaswordDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly password: string;
}
