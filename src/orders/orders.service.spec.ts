import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { OrdersService } from './orders.service';
import { MoviesService } from '../movies/movies.service';
import { OrderDetail } from './entities/order-details.entity';
import { OrderRepository } from './repositories/orders.repository';
import { BadRequestException } from '@nestjs/common';
import { User } from 'src/users/entities/users.entity';

const mockCreateOrderDetailDto = [
  {
    quantity: 2,
    movieId: 1,
  },
  {
    quantity: 5,
    movieId: 2,
  },
];

const createOrderDto = {
  orderDetails: mockCreateOrderDetailDto,
};

const mockMovieFewStock = {
  id: 1,
  stock: 4,
};

const mockMovieEnoughStock = {
  id: 1,
  stock: 10,
};

const mockOrderDuplicatedValues = [...mockCreateOrderDetailDto, { quantity: 1, movieId: 2 }];

const mockUser = {
  id: 1,
};

const mockOrder = {
  id: 1,
  orderDetails: [],
};

const mockRepository = () => ({
  save: jest.fn(),
});

const mockMoviesService = () => ({
  getMovie: jest.fn(),
  saveMovie: jest.fn(),
});

describe('Orders Service', () => {
  let ordersService: OrdersService;
  let moviesService: MoviesService;
  let orderRepository: OrderRepository;
  let orderDetailRepository: Repository<OrderDetail>;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OrdersService,
        { provide: MoviesService, useFactory: mockMoviesService },
        { provide: OrderRepository, useFactory: mockRepository },
        { provide: getRepositoryToken(OrderDetail), useFactory: mockRepository },
      ],
    }).compile();

    ordersService = module.get(OrdersService);
    moviesService = module.get(MoviesService);
    orderRepository = module.get(OrderRepository);
    orderDetailRepository = module.get(getRepositoryToken(OrderDetail));
  });
  it('Should be defined', () => {
    expect(ordersService).toBeDefined();
    expect(moviesService).toBeDefined();
    expect(orderRepository).toBeDefined();
    expect(orderDetailRepository).toBeDefined();
  });

  describe('Check Duplicated Movies', () => {
    it('Should check for duplicated movies', () => {
      const result = ordersService.containsDuplicatedMovies(mockOrderDuplicatedValues);
      expect(result).toEqual(true);
    });
  });

  describe('Check Stock Availability Movies', () => {
    it('Should check stock availability of movies', async () => {
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieEnoughStock);
      const result = await ordersService.checkStockAvailability(mockCreateOrderDetailDto);
      expect(result).toEqual(undefined);
    });
    it('Should throw error for not sufficient stock', async () => {
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieFewStock);
      expect(ordersService.checkStockAvailability(mockCreateOrderDetailDto)).rejects.toThrowError(BadRequestException);
    });
  });

  describe('Add Orders to user', () => {
    it('Should add orders to user', async () => {
      (orderRepository.save as jest.Mock).mockResolvedValue(mockOrder);
      (orderDetailRepository.save as jest.Mock).mockResolvedValue([]);
      (moviesService.getMovie as jest.Mock).mockResolvedValue(mockMovieEnoughStock);
      const result = await ordersService.addOrderToUser(mockUser as User, createOrderDto);
      expect(result).toEqual(mockOrder);
    });
  });
});
