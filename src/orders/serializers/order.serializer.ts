import { Exclude } from 'class-transformer';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';
import { User } from '../../users/entities/users.entity';
import { OrderDetail } from '../entities/order-details.entity';

export class OrderSerializer {
  @ApiProperty()
  id: number;

  @ApiProperty()
  total: number;

  @Exclude()
  @ApiHideProperty()
  user: User;

  @ApiProperty()
  orderDetails: OrderDetail[];

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  constructor(partial: Partial<OrderSerializer>) {
    Object.assign(this, partial);
  }
}
