import { IsNotEmpty, ValidateNested, ArrayNotEmpty } from 'class-validator';
import { CreateOrderDetailDto } from './create-order-detail.dto';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreateOrderDto {
  @IsNotEmpty()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  @Type(() => CreateOrderDetailDto)
  @ApiProperty({ type: [CreateOrderDetailDto] })
  orderDetails: CreateOrderDetailDto[];
}
