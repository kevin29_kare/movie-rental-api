/* istanbul ignore file */
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Check, ManyToOne } from 'typeorm';
import { IsInt, IsPositive } from 'class-validator';
import { Order } from './orders.entity';
import { Movie } from '../../movies/entities/movies.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
@Check(`"unitPrice" >= 0`)
@Check(`"subTotal" >= 0`)
export class OrderDetail {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Order,
    order => order.orderDetails,
    { nullable: false },
  )
  @ApiHideProperty()
  order: Order;

  @ManyToOne(
    () => Movie,
    movie => movie.orderDetails,
    {
      eager: true,
      nullable: false,
    },
  )
  @ApiHideProperty()
  movie: Movie;

  @Column()
  @IsInt()
  @IsPositive()
  @ApiProperty()
  quantity: number;

  @Column()
  @ApiProperty()
  unitPrice: number;

  @Column()
  @ApiProperty()
  subTotal: number;

  @ApiProperty()
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @UpdateDateColumn()
  updatedAt: Date;
}
