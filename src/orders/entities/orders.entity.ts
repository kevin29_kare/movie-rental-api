/* istanbul ignore file */
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Check,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { User } from '../../users/entities/users.entity';
import { OrderDetail } from './order-details.entity';
import { ApiProperty, ApiHideProperty } from '@nestjs/swagger';

@Entity()
@Check(`"total" >= 0`)
export class Order {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @OneToMany(
    () => OrderDetail,
    orderDetail => orderDetail.order,
  )
  @ApiProperty({ type: [OrderDetail] })
  orderDetails: OrderDetail[];

  @ManyToOne(
    () => User,
    user => user.orders,
    { nullable: false },
  )
  @ApiHideProperty()
  user: User;

  @Column()
  @ApiProperty()
  total: number;

  @CreateDateColumn()
  @ApiProperty()
  createdAt: Date;

  @UpdateDateColumn()
  @ApiProperty()
  updatedAt: Date;
}
