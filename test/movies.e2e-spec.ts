import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, forwardRef } from '@nestjs/common';
import * as request from 'supertest';
import { MoviesModule } from '../src/movies/movies.module';
import { MoviesService } from '../src/movies/movies.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

describe('MovieController (e2e)', () => {
  let app: INestApplication;
  const moviesService = {
    getMovies: () => ['Movies'],
    getMovie: (id: number) => `Movie with id ${id}`,
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot(),
        ConfigModule.forRoot({
          isGlobal: true,
        }),
        forwardRef(() => MoviesModule),
      ],
    })
      .overrideProvider(MoviesService)
      .useValue(moviesService)
      .compile();

    app = module.createNestApplication();
    await app.init();
  });

  it('/movies (GET)', () => {
    return request(app.getHttpServer())
      .get('/movies')
      .expect(200)
      .expect(moviesService.getMovies());
  });
  it('/movies/1 (GET)', () => {
    return request(app.getHttpServer())
      .get('/movies/1')
      .expect(200)
      .expect(moviesService.getMovie(1));
  });
  afterAll(async () => {
    await app.close();
  });
});
